# E-commerce Add to cart page

https://e-commerce-cart-page.netlify.app/

## Description

This project is an E-commerce Add to Cart page implemented using React.js. It consists of reusable components designed following React principles.

## Installation

To run the project locally, follow these steps:

1. Clone the repository:

   ```bash
   git clone https://github.com/karanjas39/E-commerce-cart-page.git
   ```

2. Navigate to the project directory::

   ```bash
   cd E-commerce-cart-page
   ```

3. Install dependencies:

   ```bash
    npm install
   ```

4. Start the development server:

   ```bash
    npm run dev
   ```

5. Open your browser and visit http://localhost:3000 to view the project.
