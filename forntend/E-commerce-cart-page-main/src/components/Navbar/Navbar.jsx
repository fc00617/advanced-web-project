import "./navbar.css";

function Navbar() {
  return (
    <nav>
      <div className="web-name">
        <img src="logo.png" alt="Logo Image" />
        <p>Shopify</p>
      </div>
      <div>
        <a href="#">Home</a>
        <a href="#">About</a>
        <a href="#">Shop</a>
        <a href="#">Help</a>
        <button>Your Cart</button>
      </div>
    </nav>
  );
}

export default Navbar;
