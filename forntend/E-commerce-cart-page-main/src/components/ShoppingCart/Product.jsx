import { useState } from "react";
import { RiDeleteBin6Line } from "react-icons/ri";

function Product({
  img,
  name,
  des,
  price,
  onSetTotalBill,
  onSetProducts,
  id,
  count,
}) {
  const [productCount, setProductCount] = useState(count);

  function handleProductIncrease() {
    setProductCount((prevCount) => prevCount + 1);
    onSetProducts((prevProducts) =>
      prevProducts.map((product) =>
        product.id == id ? { ...product, count: productCount + 1 } : product
      )
    );
    onSetTotalBill((prevBill) => prevBill + price);
  }

  function handleProductDecrease() {
    if (productCount === 1) return;
    setProductCount((prevCount) => prevCount - 1);
    onSetProducts((prevProducts) =>
      prevProducts.map((product) =>
        product.id == id ? { ...product, count: productCount - 1 } : product
      )
    );
    onSetTotalBill((prevBill) => prevBill - price);
  }

  function handleRemoveItem() {
    onSetTotalBill((prevBill) => prevBill - price * productCount);
    onSetProducts((prevProducts) =>
      prevProducts.filter((product) => product.id != id)
    );
  }

  return (
    <div className="cartProduct">
      <div>
        <img src={img} alt={name + " image"} />
        <div>
          <p>{name}</p>
          <p>{des}</p>
          <p>${price}</p>
        </div>
      </div>
      <div className="productCounter">
        <p>{productCount} pcs</p>
        <p>
          <button onClick={handleProductIncrease}>+</button>
          <button onClick={handleProductDecrease}>-</button>
        </p>
      </div>
      <div className="shpCartBtns">
        <button className="removeProduct" onClick={handleRemoveItem}>
          Remove Item <RiDeleteBin6Line />
        </button>
      </div>
    </div>
  );
}

export default Product;
