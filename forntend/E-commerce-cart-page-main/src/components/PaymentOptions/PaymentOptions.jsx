import { useState } from "react";

import PaymentForm from "./PaymentForm";
import ShippingSummary from "../ShippingDetails/ShippingSummary";

import "./paymentOptions.css";

function PaymentOptions({
  totalBill,
  tax,
  products,
  shippingCost,
  paymentMode,
  onSetPaymentMode,
}) {
  return (
    <div className="shpPaymentMain">
      <PaymentForm
        paymentMode={paymentMode}
        onSetPaymentMode={onSetPaymentMode}
      />
      <ShippingSummary
        totalBill={totalBill}
        tax={tax}
        products={products}
        shippingCost={shippingCost}
      />
      <div className="shpCartBtns">
        <button>Pay Now</button>
        <button>Cancel</button>
      </div>
    </div>
  );
}

export default PaymentOptions;
