import { useEffect } from "react";
import "./footer.css";
import { useState } from "react";

function Footer() {
  const [currentYear, setCurrentYear] = useState(new Date().getFullYear());

  useEffect(() => {
    setCurrentYear(new Date().getFullYear());
  }, []);

  useEffect(() => {}, []);
  return (
    <footer>
      <p>Copyright &copy; {currentYear} | All rights reserved.</p>
    </footer>
  );
}

export default Footer;
