import ShippingDetailForm from "./ShippingDetailForm";
import ShippingSummary from "./ShippingSummary";

import "./shippingDetails.css";

function ShippingDetails({
  onSetActiveIndex,
  totalBill,
  tax,
  products,
  shippingCost,
  onSetShippingCost,
  onSetTotalBill,
  countries,
}) {
  return (
    <div className="shpDetailMain">
      <ShippingDetailForm
        onSetShippingCost={onSetShippingCost}
        shippingCost={shippingCost}
        onSetTotalBill={onSetTotalBill}
        countries={countries}
      />
      <ShippingSummary
        totalBill={totalBill}
        tax={tax}
        products={products}
        shippingCost={shippingCost}
      />
      <div className="shpCartBtns">
        <button onClick={() => onSetActiveIndex((a) => (a == 2 ? 2 : a + 1))}>
          Next
        </button>
        <button>Cancel</button>
      </div>
    </div>
  );
}

export default ShippingDetails;
