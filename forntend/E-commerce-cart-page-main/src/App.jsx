import { useState, useEffect } from "react";

import Navbar from "./components/Navbar/Navbar";
import ShoppingTabLinks from "./components/ShoppingTabLinks/ShoppingTabLinks";
import Footer from "./components/Footer/Footer";

import ShoppingCart from "./components/ShoppingCart/ShoppingCart";
import ShippingDetails from "./components/ShippingDetails/ShippingDetails";
import PaymentOptions from "./components/PaymentOptions/PaymentOptions";

function App({ tax, product }) {
  const [activeIndex, setActiveIndex] = useState(0);
  const [totalBill, setTotalBill] = useState(0);
  const [shippingCost, setShippingCost] = useState(0);
  const [products, setProducts] = useState([]);
  const [paymentMode, setPaymentMode] = useState(1);
  const [countries, setCountries] = useState([]);

  useEffect(() => {
    const bill = products.reduce(
      (acc, product) => acc + product.price * product.count,
      0
    );
    setTotalBill(bill + tax);
    const fetchData = async () => {
      try {
        const response = await fetch(
          "https://gist.githubusercontent.com/kalinchernev/486393efcca01623b18d/raw/daa24c9fea66afb7d68f8d69f0c4b8eeb9406e83/countries"
        );
        const data = await response.text();
        const countriesArray = data
          .split("\n")
          .filter((country) => country.trim() !== "");
        setCountries(["Select your country", ...countriesArray]);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };


    const fetchProducts = async () => {
      try {
        const response = await fetch(
          "https://apionthefly.vercel.app/api/endpoints/sampler"
        );
        const data = await response.json();
        setProducts(data.data)
        console.log('the data',data)

      } catch (error) {
        console.error("Error fetching Products:", error);
      }
    };

    fetchData();
    fetchProducts();
  }, []);

  return (
    <>
      <Navbar />
      <ShoppingTabLinks
        links={["Shopping Cart", "Shipping Details", "Payment Options"]}
        activeIndex={activeIndex}
        onSetActiveIndex={setActiveIndex}
      />
      {activeIndex == 0 && (
        <ShoppingCart
          products={products}
          onSetProducts={setProducts}
          tax={tax}
          onSetActiveIndex={setActiveIndex}
          onSetTotalBill={setTotalBill}
          totalBill={totalBill}
          shippingCost={shippingCost}
        />
      )}
      {activeIndex == 1 && (
        <ShippingDetails
          onSetActiveIndex={setActiveIndex}
          totalBill={totalBill}
          tax={tax}
          products={products}
          shippingCost={shippingCost}
          onSetTotalBill={setTotalBill}
          onSetShippingCost={setShippingCost}
          countries={countries}
        />
      )}
      {activeIndex == 2 && (
        <PaymentOptions
          totalBill={totalBill}
          tax={tax}
          products={products}
          shippingCost={shippingCost}
          paymentMode={paymentMode}
          onSetPaymentMode={setPaymentMode}
        />
      )}
      <Footer />
    </>
  );
}

export default App;
