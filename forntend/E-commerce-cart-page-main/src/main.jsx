import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";

const products = [
  {
    img: "/p1.jpg",
    name: "Battlecreek Coffee",
    des: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Integer malesuada ",
    price: 160,
    count: 5,
    id: 1,
  },
  {
    img: "/p3.jpg",
    name: "Sports Cycle",
    des: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Integer malesuada nunc vel risus commodo viverra. Ipsum dolor sit amet consectetur adipiscing. Tristique nulla aliquet enim tortor at auctor. Malesuada fames ac turpis egestas maecenas pharetra convallis ",
    price: 470,
    count: 1,
    id: 2,
  },
  {
    img: "/p4.jpg",
    name: "Apple watch",
    des: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Integer malesuada ",
    price: 550,
    count: 1,
    id: 3,
  },
  {
    img: "/p5.jpg",
    name: "B & O Headphone",
    des: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do  malesuada ",
    price: 70,
    count: 1,
    id: 4,
  },
];

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <App product={products} tax={20} />
  </React.StrictMode>
);
