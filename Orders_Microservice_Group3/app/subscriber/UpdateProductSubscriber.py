import sys
print(sys.path)
from threading import Thread
from kafka import KafkaConsumer
import json
import logging
from config import KAFKA_SERVER

#KAFKA_SERVER= "localhost:9092"
TOPIC_NAME = "product_updated_topic"


from  models.UpdateProduct import UpdateProduct

def consume_product_updated_event():
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    logging.info("Consuming product updated event...")
    
    # Create KafkaConsumer listening on the product_updated_topic
    consumer = KafkaConsumer("product_updated_topic", bootstrap_servers=KAFKA_SERVER)
    
    for message in consumer:
        product_data_str = message.value.decode("utf-8")
        product_data = json.loads(product_data_str)
        logging.info("Received product update: {}".format(product_data))
        
        # Call your UpdateProduct function passing the dictionary containing product details
        UpdateProduct(product_id=product_data['product_id'], 
                      quantity=product_data['quantity'], 
                      price=product_data['price'])

def start_kafka_consumer():
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    logging.info("Starting Kafka consumer...")

    # Starting the consumer in a separate thread
    kafka_thread = Thread(target=consume_product_updated_event)
    kafka_thread.daemon = True  
    kafka_thread.start()