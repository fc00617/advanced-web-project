from models.database_connection import connect_db
import pyodbc

from decimal import Decimal



def check_availability_and_price(product_id, requested_quantity, expected_price, user_id):
    connection = connect_db()
    try:
        cursor = connection.cursor()
        # Fetch the current available quantity and price
        cursor.execute("""
            SELECT QuantityAvailable, FORMAT(UnitPrice, 'N2') AS UnitPrice
            FROM ProductData
            WHERE ProductID = ?
        """, (product_id,))

        row = cursor.fetchone()
        if row:
            available_quantity, current_price = row
            current_pricefloat = float(current_price)  # Convert formatted price back to float if necessary for comparisons

            response_data = {
                "available_quantity": available_quantity,
                "current_price": current_price,
                "available": available_quantity >= requested_quantity,
                "price_correct": current_pricefloat == expected_price
            }

            messages = []
            if not response_data["available"]:
                messages.append(f"We apologize, but there are only {available_quantity} items available.")

            # Check if the price has changed
            if not response_data["price_correct"]:
                messages.append(f"The price has changed. The current price is now {current_pricefloat}.")
                # Update the ShoppingCart with the new price
                cursor.execute("""
                    UPDATE ShoppingCart
                    SET UnitPrice = ?
                    WHERE ProductID = ? AND UserID = ?
                """, (current_price, product_id, user_id))  
                connection.commit()

            if messages:
                response_data["message"] = " ".join(messages)
                return response_data
            else:
                # All checks passed
                return response_data

        else:
            return {"available": False, "message": "Product not found."}

    except pyodbc.Error as e:
        print(f"Database error in check_availability_and_price: {e}")
        connection.rollback()
        return {"available": False, "message": f"Database error: {str(e)}"}

    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()

