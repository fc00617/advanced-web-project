import pyodbc
from models.database_connection import connect_db

def fetch_order_history(user_id):
    try:
        # Open database connection
        connection = connect_db()
        cursor = connection.cursor()

        # Fetch the order history for the user
        cursor.execute("""
            SELECT og.OrderGroupID, og.CustomerID, og.TransactionDate, og.Location, og.OrdersStatus, og.TotalPrice, oi.OrderItemID, oi.ProductID, oi.UnitPrice, oi.Quantity, oi.TotalItemPrice
            FROM OrderGroup og
            INNER JOIN OrderItem oi ON og.OrderGroupID = oi.OrderGroupID
            WHERE og.CustomerID = ?
            ORDER BY og.TransactionDate DESC;""", (user_id,))

        # Fetch all records and close cursor and connection
        records = cursor.fetchall()
        cursor.close()
        connection.close()

        # Process the fetched data into a structured format for the API response
        order_history = {}
        for record in records:
            # Unpack the fetched row
            order_group_id, customer_id, transaction_date, location, orders_status, total_price, order_item_id, product_id, unit_price, quantity, total_item_price = record

            
            if order_group_id not in order_history:
                order_history[order_group_id] = {
                    "CustomerID": customer_id,
                    "TransactionDate": transaction_date,
                    "Location": location,
                    "OrdersStatus": orders_status,
                    "TotalPrice": total_price,
                    "Items": []
                }

            # Append this item to the items list for the order group
            order_history[order_group_id]["Items"].append({
                "OrderItemID": order_item_id,
                "ProductID": product_id,
                "UnitPrice": unit_price,
                "Quantity": quantity,
                "TotalItemPrice": total_item_price
            })

        # Convert to list of order histories
        order_history_list = list(order_history.values())
        return order_history_list

    except pyodbc.Error as e:
        # Close cursor and connection in case of error
        if cursor:
            cursor.close()
        if connection:
            connection.close()
        return {"error": str(e)}
