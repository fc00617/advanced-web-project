#from confluent_kafka import Producer
import json
import pyodbc
from flask import jsonify
#from flask import jsonify
from models.database_connection import connect_db

#subscriber

def UpdateProduct(product_id, quantity, price):
    try:
        connection = connect_db()
        connection.autocommit = False
        cursor = connection.cursor()

        # SQL MERGE statement to insert or update
        sql_product_data  = """
        MERGE INTO ProductData AS target
        USING (SELECT ? AS ProductID, ? AS QuantityAvailable, ? AS UnitPrice) AS source
        ON target.ProductID = source.ProductID
        WHEN MATCHED THEN
            UPDATE SET QuantityAvailable = source.QuantityAvailable, UnitPrice = source.UnitPrice
        WHEN NOT MATCHED BY TARGET THEN
            INSERT (ProductID, QuantityAvailable, UnitPrice)
            VALUES (source.ProductID, source.QuantityAvailable, source.UnitPrice);
        """
        cursor.execute(sql_product_data, (product_id, quantity, price))

        # Update ShoppingCart to reflect new prices
        sql_shopping_cart = """
        UPDATE ShoppingCart
        SET UnitPrice = ?
        WHERE ProductID = ?
        """
        cursor.execute(sql_shopping_cart, (price, product_id))

        connection.commit()
        return {"success": "Product information updated successfully."}

    except pyodbc.Error as e:
        print(f"Database error during product update: {e}")
        connection.rollback()
        return {"error": f"Database error during product update: {e}"}

    except Exception as e:
        print(f"Unexpected error occurred: {e}")
        connection.rollback()
        return {"error": f"Unexpected error: {e}"}

    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()

