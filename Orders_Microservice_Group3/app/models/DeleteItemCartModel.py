from flask import jsonify
from models.database_connection import connect_db
import pyodbc

def delete_item_from_cart(user_id, cart_item_id):

    try:  # error handling
        connection = connect_db()
        cursor = connection.cursor()

        # SQL query to delete the item from the cart based on cart_item_id and UserID
        delete_cart_item_query = '''DELETE FROM dbo.ShoppingCart WHERE UserID = ? AND CartItemID = ?;'''
        cursor.execute(delete_cart_item_query, (user_id, cart_item_id))

        # Committing the transaction to the database
        connection.commit()

        # Check if the delete operation was successful
        if cursor.rowcount == 0:
            return {"error": "Item not found or could not be deleted"}

        return {"message": "Item deleted successfully"}

    except pyodbc.Error as e:
        print(f"Database error in delete_item_from_cart: {e}")
        # Rollback the transaction in case of an error
        connection.rollback()
        return {"error": "Database error during item deletion"}

    except Exception as e:
        print(f"Unexpected error occurred in delete_item_from_cart: {e}")
        # Rollback the transaction in case of an unexpected error
        connection.rollback()
        return {"error": "Unexpected error during item deletion"}

    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()