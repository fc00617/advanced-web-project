import pyodbc
from datetime import datetime
from models.database_connection import connect_db


from models.database_connection import connect_db
from flask import jsonify
import pyodbc

def get_user_cart_items(user_id):
    try:
        connection = connect_db()
        cursor = connection.cursor()

        query = "SELECT * FROM ShoppingCart WHERE UserID = ?"
        cursor.execute(query, (user_id,))

        # Fetch all rows for the user_id
        rows = cursor.fetchall()

        # Prepare a list of dictionaries to hold cart item data
        cart_items = []
        Total_Price = 0
        for row in rows:
            Total_item_price = row.CartQuantity * row.UnitPrice
            cart_item = {
                "CartItemID": row.CartItemID, 
                "UserID": row.UserID,
                "ProductID": row.ProductID,
                "UnitPrice": row.UnitPrice,
                "CartQuantity": row.CartQuantity,
                "AddedDate": row.AddedDate,
                "TotalItemPrice": Total_item_price
            }
            cart_items.append(cart_item)
            Total_Price += Total_item_price

        return {"CartItems": cart_items, "TotalPrice": Total_Price}

    except pyodbc.Error as e:
        print(f"Database error in get_user_cart_items: {e}")
        return {"error": "Database error"}

    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()