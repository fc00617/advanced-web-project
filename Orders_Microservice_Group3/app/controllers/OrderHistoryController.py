from flask import Blueprint, jsonify, session
from models.order_history import fetch_order_history

order_history_bp = Blueprint('order_history', __name__)

@order_history_bp.route('/order/history', methods=['GET'])
def order_history():
    user_id = session.get('user_id')
    #user_id = 1
    if not user_id:
        return jsonify({"error": "Authentication required to view order history."}), 401
    
    order_history_data = fetch_order_history(user_id)
    
    if 'error' in order_history_data:
        return jsonify({"error": order_history_data["error"]}), 500

    return jsonify({"order_history": order_history_data}), 200