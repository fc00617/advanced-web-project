from models.DeleteItemCartModel import delete_item_from_cart
from flask import Blueprint, jsonify, request, json, session

DeleteCart_bp = Blueprint("delete", __name__)



@DeleteCart_bp.route("/cart/delete/<int:cart_item_id>", methods=['DELETE'])
def delete_item(cart_item_id):
    #user_id = 1
    user_id = session.get("user_id")
    
    if not user_id:
        return jsonify({"error": "You need to be logged in to delete items from the cart."}), 401

    # Call the function to delete the item from the cart
    result = delete_item_from_cart(user_id, cart_item_id)

    if "error" in result:
        return jsonify(result), 404  
    return jsonify(result), 200