import logging
from .app.subscriber.UpdateProductSubscriber import consume_product_updated_event

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    consume_product_updated_event()