# from flask import Flask
# from app.__init__ import create_app
# app = Flask(__name__)

# # Define the route for the root URL "/"
# @app.route('/')
# def home():
#     # This is the view function that returns a response
#     return "Welcome to the Flask App!"

# # Check if this is the script being run as the main program and start the app
# if __name__ == '__main__':
#     app.run(debug=True)

# from flask import Flask
# from flask_cors import CORS


# #from app.models import models

# app = Flask(__name__)
# CORS(app)

# #db = sqlAlchemy

# #from app import routes

# if __name__ == '__main__':
#     app.run(debug=True)

from app import create_app  # Adjusted import to reference the app package

app = create_app()

if __name__ == '__main__':
    app.run(debug=True)