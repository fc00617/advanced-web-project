from flask import Blueprint, jsonify, request, json, session
from models.FetchKart import get_user_cart_items
import requests

# cart_bp = Blueprint("cart", __name__)

# @cart_bp.route("/cart", methods=["GET"])
# def fetch_user_cart():
#     user_id = session.get("user_id")

#     if not user_id:
#         return jsonify({"error": "You need to be logged in to view the cart."}), 401

#     cart_data = get_user_cart_items(user_id)
#     if "error" in cart_data:
#         return jsonify({"error": cart_data["error"]}),

#     return jsonify(cart_data)

#This code is to test

cart_bp = Blueprint("cart", __name__)

@cart_bp.route("/cart", methods=["GET"])
def fetch_user_cart():
    
    user_id = 5

    if not user_id:
        return jsonify({"error": "User ID is required."}), 400

    cart_data = get_user_cart_items(user_id)
    if "error" in cart_data:
        return jsonify({"error": cart_data["error"]}), 500

    return jsonify(cart_data)