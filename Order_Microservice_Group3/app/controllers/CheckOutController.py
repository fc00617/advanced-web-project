from flask import Blueprint, request, jsonify, session
from models.CheckOut import checkout_cart
from publishers.kafkaPublishers import publish_product_updated_event

checkout_bp = Blueprint('checkout', __name__)

@checkout_bp.route('/cart/checkout', methods=['POST'])
def handle_checkout():
    #user_id = session.get("user_id")  # Retrieve the user ID from session; ensure the user is logged in
    user_id = 1
    if not user_id:
        return jsonify({"error": "User not logged in"}), 401

    data = request.get_json()
    if not data or 'address' not in data:
        return jsonify({"error": "Missing required fields: address"}), 400

    address = data['address']

    # Call the checkout function with the provided data
    checkout_result = checkout_cart(user_id, address)

    # Check for errors in the result and return appropriate responses
    if 'error' in checkout_result:
        return jsonify({"error": checkout_result["error"]}), 500  # or another appropriate status code

    # If checkout is successful, publish an event to Kafka for each item purchased
    for item in checkout_result.get('item_details', []):  # Ensure that item_details are included in checkout_result
        event_data = {
            "ProductID": item['ProductID'],
            "QuantityPurchased": item['Quantity']
        }
        publish_product_updated_event(event_data)

    return jsonify({
        "message": "Checkout completed successfully",
        "TransactionID": checkout_result.get("TransactionID", "Unknown")
    }), 200