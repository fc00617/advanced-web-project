from flask import Blueprint, jsonify, request, json, session

from models.AddToCart import add_item_shopping_cart
from models.CheckPriceAndQuantity import check_availability_and_price

import requests

AddToCart_bp = Blueprint("AddToCart", __name__)

#user_id = session.get("user_id")

@AddToCart_bp.route("/cart/AddToCart", methods=["POST"])
def add_item():
    user_id = 1  # Temporarily bypass authentication for testing

    data = request.get_json()
    ProductID = data.get("ProductID")
    CartQuantity = data.get("CartQuantity")
    UnitPrice = data.get("UnitPrice")

    # Check for required data
    if not ProductID or not CartQuantity or UnitPrice is None:
        return jsonify({"error": "Product ID, quantity, and unit price are required."}), 400

    # Call the CheckPriceAndQuantity function
    inventory_check = check_availability_and_price(ProductID, CartQuantity, UnitPrice, user_id)

    # If there's an issue with availability or price, return the message from the inventory check
    if not inventory_check.get("available") or not inventory_check.get("price_correct"):
        return jsonify({
            "error": inventory_check.get("message"),  # Message from inventory check
            "available_quantity": inventory_check.get("available_quantity"),
            "current_price": inventory_check.get("current_price")
        }), 400

    cart_item_data = {
        "UserID": user_id,
        "ProductID": ProductID,
        "UnitPrice": UnitPrice,
        "CartQuantity": CartQuantity
    }

    # Update cart item data with the current price
    cart_item_data["UnitPrice"] = inventory_check["current_price"]

    # If the checks pass, add the item to the shopping cart
    add_cart_item_message = add_item_shopping_cart(cart_item_data)
    return jsonify(add_cart_item_message)
