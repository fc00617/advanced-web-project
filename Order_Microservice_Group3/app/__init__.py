# from flask import Flask

# from .controllers.AddToCartController import AddToCart_bp
# from .controllers.CheckOutController import checkout_bp
# from .controllers.DeleteFromItemController import DeleteCart_bp
# from .controllers.FetchCartController import cart_bp


# def create_app():
#     app = Flask(__name__)
#     app.register_blueprint(AddToCart_bp)
#     app.register_blueprint(checkout_bp)
#     app.register_blueprint(DeleteCart_bp)
#     app.register_blueprint(cart_bp)


#     return app

from flask import Flask
from flask_cors import CORS
import os

from app.controllers.AddToCartController import AddToCart_bp
from app.controllers.CheckOutController import checkout_bp
from .controllers.DeleteFromItemController import DeleteCart_bp
from .controllers.FetchCartController import cart_bp
from .subscriber.UpdateProductSubscriber import start_kafka_consumer

def create_app():
    app = Flask(__name__)
    CORS(app)
    
    app.config.from_object('config')

    app.register_blueprint(AddToCart_bp)
    app.register_blueprint(checkout_bp)
    app.register_blueprint(DeleteCart_bp)
    app.register_blueprint(cart_bp)

    # Optionally start Kafka consumer based on configuration
    if app.config.get('START_KAFKA_CONSUMER', False):
        start_kafka_consumer()

    return app