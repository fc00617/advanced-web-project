print("Subscribers package initialized")

from subscriber.UpdateProductSubscriber import consume_product_updated_event

from subscriber.UpdateProductSubscriber import start_kafka_consumer
