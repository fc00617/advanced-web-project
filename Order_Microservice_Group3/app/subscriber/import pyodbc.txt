import pyodbc
from datetime import datetime
from app.models.database_connection import connect_db
from your_kafka_module import kafka_produce  # Import the Kafka producer function

def order_service_consumer():
    consumer = Consumer({
        'bootstrap.servers': 'localhost:9092',
        'group.id': 'orders-group',
        'auto.offset.reset': 'earliest'
    })
    consumer.subscribe(['product_changes'])

    while True:
        message = consumer.poll(1.0)
        if message is None:
            continue
        if message.error():
            print("Consumer error: {}".format(message.error()))
            continue

        data = json.loads(message.value().decode('utf-8'))
        if data['type'] == 'quantity_update':
            # Update orders database based on quantity change
            update_orders_database(data['product_id'], data['quantity_sold'])

def update_orders_database(product_id, quantity_sold):
    connection = connect_db()
    try:
        cursor = connection.cursor()
        # Update logic here, e.g., flagging orders as fulfilled
        cursor.execute("UPDATE Orders SET Status = 'Updated' WHERE product_id = ?", (product_id,))
        connection.commit()
    finally:
        cursor.close()
        connection.close()








kafka-console-producer.bat --broker-list localhost:9092 --topic product_update_topic
