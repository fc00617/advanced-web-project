from kafka import KafkaProducer
from kafka.admin import KafkaAdminClient, NewTopic

import json

producer = KafkaProducer(bootstrap_servers="localhost:9092")


#Creates the topic
def create_Quantity_updated_topic():
    # Create KafkaAdminClient instance
    admin_client = KafkaAdminClient(bootstrap_servers="localhost:9092")

    # Define the topic name and configuration
    topic_name = "QuantityUpdateFromOrders"
    num_partitions = 1
    replication_factor = 1

    # Retrieve the list of existing topics
    topic_metadata = admin_client.list_topics()

    # Check if the topic already exists
    if topic_name not in topic_metadata:
        # Define the configuration for the new topic
        new_topic = NewTopic(name=topic_name, num_partitions=num_partitions, replication_factor=replication_factor)
        # Create the new topic
        admin_client.create_topics(new_topics=[new_topic], validate_only=False)


def publish_product_updated_event(event_data):
    # Serialize the event data to JSON
    event_json = json.dumps(event_data)
    # Publish the event to the Kafka topic
    data_to_send = producer.send("QuantityUpdateFromOrders", value=event_json.encode("utf-8"))
    try:
        record_metadata = data_to_send.get(timeout=10)
        print("Message sent successfully!")
        print("Topic:", record_metadata.topic)
        print("Partition:", record_metadata.partition)
        print("Offset:", record_metadata.offset)
    except Exception as e:
        print("Failed to send message:", e)
    producer.flush()
