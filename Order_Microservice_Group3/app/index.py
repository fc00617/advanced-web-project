from flask import Flask, jsonify, request, render_template, session, abort, redirect, url_for, make_response
import requests
import subscriber
from flask_cors import CORS
from config import DEBUG, SECRET_KEY, PORT, HOST
import os

# Importing blueprints from the controllers
from controllers.AddToCartController import AddToCart_bp
from controllers.CheckOutController import checkout_bp
from controllers.DeleteFromItemController import DeleteCart_bp
from controllers.FetchCartController import cart_bp
from controllers.OrderHistoryController import order_history_bp

# Setting up Flask application
app = Flask(__name__)
CORS(app)

# Handling secret key based on the execution environment
try:
    # Checking if running inside a Docker container
    with open("/proc/self/cgroup", "r") as cgroup_file:
        cgroup_info = cgroup_file.read()

    if 'docker' in cgroup_file.read():
        print("Running inside Docker container")
        app.secret_key = os.environ.get('SECRET_KEY', SECRET_KEY)  # Fallback to default if env var is missing

except FileNotFoundError:
    print("Running on a non-Linux system or cgroup not found")
    app.secret_key = SECRET_KEY  # Use the default secret key if the file is not found


@app.route('/cart', methods=['POST'])
def get_session_id():
    session_id = session.get('user_id')
    if session_id:
        return jsonify({'session_id': session_id})
    else:
        return jsonify({'message': 'Session ID not found'})

# @app.route('/')
# def index():
    
#     return render_template("index.html")


# Register blueprints for different parts of the application
app.register_blueprint(AddToCart_bp)
app.register_blueprint(checkout_bp)
app.register_blueprint(DeleteCart_bp)
app.register_blueprint(cart_bp)
app.register_blueprint(order_history_bp)

if __name__ == '__main__':

    subscriber.start_kafka_consumer()

    # Running the Flask application
    app.run(debug=DEBUG, port=PORT)









# from flask import Flask, redirect, url_for, request, render_template, make_response, session, abort
# from flask_cors import CORS
# from flask import jsonify
# from config import DEBUG, SECRET_KEY, PORT
# import os
# import requests
# import subscribers

# from controllers.AddToCartController import AddToCart_bp
# from controllers.CheckOutController import checkout_bp
# from controllers.DeleteFromItemController import DeleteCart_bp
# from controllers.FetchCartController import cart_bp
# from controllers.UpdateProductController import cart_bp


# app = Flask(__name__)
# CORS(app)


# app.secret_key = SECRET_KEY


# # Read user microservice URL from environment variable
# USER_MICROSERVICE_URL = os.getenv('USER_MICROSERVICE_URL', 'http://127.0.0.1:5000')


# @app.route('/cart', methods=['POST'])
# def get_session_id():
#     session_id = session.get('user_id')
#     if session_id:
#         return jsonify({'session_id': session_id})
#     else:
#         return jsonify({'message': 'Session ID not found'})

# @app.route('/')
# def index():
#     return render_template("index.html")



# # app.register_blueprint(AddToCart_bp)
# # app.register_blueprint(checkout_bp)
# # app.register_blueprint(DeleteCart_bp)
# # app.register_blueprint(cart_bp)



# if __name__ == '__main__':

#     subscribers.start_kafka_consumer()

#     app.run(debug=DEBUG, port=PORT)