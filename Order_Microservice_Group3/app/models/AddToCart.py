import pyodbc
from flask import jsonify
from decimal import Decimal
from models.database_connection import connect_db

def add_item_shopping_cart(data):

    try: #error handling
        connection = connect_db()
        cursor = connection.cursor()

        insert_cart_item_query = '''INSERT INTO dbo.ShoppingCart (UserID, ProductID, UnitPrice, CartQuantity)
VALUES (?, ?, ?, ?);'''
        cursor.execute(insert_cart_item_query, (data["UserID"], data["ProductID"], data["UnitPrice"], data["CartQuantity"]))
 
        connection.commit()

        return {"message": "Item added to cart successfully"}

    except pyodbc.Error as e:
        print(f"Database error in add_item_to_cart: {e}")
        connection.rollback()
        return {"Error": f"Unexpected error: {str(e)}"}

    except Exception as e:
        print(f"Unexpected error occurred in add_item_to_cart: {e}")
        connection.rollback()
        return {"Error": "Unexpected error"}

    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()

