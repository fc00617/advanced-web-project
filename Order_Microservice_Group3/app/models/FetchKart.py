import pyodbc
from datetime import datetime
from models.database_connection import connect_db

# def checkout_cart(user_id, cart_items, Location, total_price):
#     try:
#         connection = connect_db()
#         # Ensure that all operations are executed as a single transaction
#         connection.autocommit = False
#         cursor = connection.cursor()

#         # Insert a new order group and capture the Transaction_ID using the OUTPUT clause
#         insert_order_group_stmt = """
#             INSERT INTO OrderGroup (CustomerID, TotalPrice, TransactionDate, Location)
#             OUTPUT INSERTED.OrderGroupID
#             VALUES (?, ?, ?, ?, ?)
#         """
#         cursor.execute(insert_order_group_stmt, (user_id, total_price, datetime.now(), Location))
#         OrderGroupID = cursor.fetchone()[0]  # Fetch the Transaction_ID returned by the OUTPUT clause

#         # Insert cart items into the OrderItem table and link them to the new order group
#         for item in cart_items:
#             cursor.execute("""
#                 INSERT INTO OrderItem (OrderGroupID, StockCode, OrdersStatus, Quantity, TotalItemPrice)
#                 VALUES (?, ?, ?, ?, ?)
#             """, (OrderGroupID, item['ProductID'], 'Processing', item['CartQuantity'], item['TotalItemPrice']))

#         # Delete the cart items from the ShoppingCart table
#         cursor.execute("DELETE FROM ShoppingCart WHERE UserID = ?", (user_id,))

#         # Commit the transaction
#         connection.commit()

#         return {"success": "Checkout completed successfully.", "OrderID": OrderGroupID}

#     except pyodbc.Error as e:
#         print(f"Database error during checkout: {e}")
#         connection.rollback()  # Rollback in case of an error
#         return {"error": "Database error during checkout"}

#     finally:
#         if cursor:
#             cursor.close()
#         if connection:
#             connection.close()


from models.database_connection import connect_db
from flask import jsonify
import pyodbc

def get_user_cart_items(user_id):
    try:
        connection = connect_db()
        cursor = connection.cursor()

        query = "SELECT * FROM ShoppingCart WHERE UserID = ?"
        cursor.execute(query, (user_id,))

        # Fetch all rows for the user_id
        rows = cursor.fetchall()

        # Prepare a list of dictionaries to hold cart item data
        cart_items = []
        Total_Price = 0
        for row in rows:
            Total_item_price = row.CartQuantity * row.UnitPrice
            cart_item = {
                "CartItemID": row.CartItemID, 
                "UserID": row.UserID,
                "ProductID": row.ProductID,
                "UnitPrice": row.UnitPrice,
                "CartQuantity": row.CartQuantity,
                "AddedDate": row.AddedDate,
                "TotalItemPrice": Total_item_price
            }
            cart_items.append(cart_item)
            Total_Price += Total_item_price

        return {"CartItems": cart_items, "TotalPrice": Total_Price}

    except pyodbc.Error as e:
        print(f"Database error in get_user_cart_items: {e}")
        return {"error": "Database error"}

    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()




#for interacting with the message broker, also i need to do it for price

# def can_fulfill_order(user_id):
#     try:
#         connection = connect_db()
#         cursor = connection.cursor()

#         # Assuming you have a function that gets the latest stock level for a given product ID
#         def get_stock_level(product_id):
#             # This function should query your cache or database that is kept up to date with Kafka messages
#             # For demonstration, let's assume it queries a database table `ProductStock`
#             cursor.execute("SELECT StockLevel FROM ProductStock WHERE ProductID = ?", (product_id,))
#             stock_info = cursor.fetchone()
#             return stock_info.StockLevel if stock_info else 0

#         query = "SELECT * FROM ShoppingCart WHERE UserID = ?"
#         cursor.execute(query, (user_id,))
#         rows = cursor.fetchall()

#         cart_items = []
#         Total_Price = 0
#         for row in rows:
#             current_stock_level = get_stock_level(row.ProductID)
#             if row.CartQuantity > current_stock_level:
#                 # If any item exceeds the available stock, return immediately indicating the order cannot be fulfilled
#                 return {"error": f"Cannot fulfill order for ProductID {row.ProductID}. Only {current_stock_level} left in stock."}

#             Total_item_price = row.CartQuantity * row.UnitPrice
#             cart_item = {
#                 "CartItemID": row.CartItemID, 
#                 "UserID": row.UserID,
#                 "ProductID": row.ProductID,
#                 "UnitPrice": row.UnitPrice,
#                 "CartQuantity": row.CartQuantity,
#                 "AddedDate": row.AddedDate,
#                 "TotalItemPrice": Total_item_price
#             }
#             cart_items.append(cart_item)
#             Total_Price += Total_item_price

#         # If all items can be fulfilled, return the cart items and total price
#         return {"CartItems": cart_items, "TotalPrice": Total_Price}

#     except pyodbc.Error as e:
#         print(f"Database error in can_fulfill_order: {e}")
#         return {"error": "Database error"}

#     finally:
#         if cursor:
#             cursor.close()
#         if connection:
#             connection.close()