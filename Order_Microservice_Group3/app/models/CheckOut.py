from models.database_connection import connect_db
from flask import Blueprint, request, jsonify, session

from decimal import Decimal
import pyodbc
from datetime import datetime



def checkout_cart(user_id, address):
    try:
        connection = connect_db()
        connection.autocommit = False
        cursor = connection.cursor()

        # Fetch cart items for the user
        cursor.execute("""
            SELECT ProductID, CartQuantity, FORMAT(UnitPrice, 'N2') AS UnitPrice
            FROM ShoppingCart
            WHERE UserID = ?
        """, (user_id,))
        cart_items = cursor.fetchall()

        if not cart_items:
            return {"error": "No items in the cart to checkout."}

        total_price = 0
        item_details = []

        # Verify availability and price for each cart item
        for item in cart_items:
            ProductID, CartQuantity, UnitPrice = item
            UnitPrice_decimal = Decimal(UnitPrice)  # Convert string to Decimal for precise calculations

            # Fetch the current availability and unit price from the product data
            cursor.execute("""
                SELECT QuantityAvailable, FORMAT(UnitPrice, 'N2') AS UnitPrice
                FROM ProductData
                WHERE ProductID = ?
            """, (ProductID,))
            row = cursor.fetchone()
            if row:
                available_quantity, current_price = row
                current_price_decimal = Decimal(current_price)

                if available_quantity < CartQuantity:
                    connection.rollback()
                    return {"error": f"Not enough inventory for ProductID {ProductID}."}

                if current_price_decimal != UnitPrice_decimal:
                    cursor.execute("""
                        UPDATE ShoppingCart
                        SET UnitPrice = ?
                        WHERE ProductID = ? AND UserID = ?
                    """, (current_price, ProductID, user_id))
                    connection.commit()
                    return {"error": f"Price has changed for ProductID {ProductID}. Updated price in cart."}

                                
                new_quantity = available_quantity - CartQuantity
                cursor.execute("""
                    UPDATE ProductData
                    SET QuantityAvailable = ?
                    WHERE ProductID = ?
                """, (new_quantity, ProductID))


                TotalItemPrice = CartQuantity * current_price_decimal
                total_price += TotalItemPrice
                item_details.append({
                    "ProductID": ProductID,
                    "Quantity": CartQuantity,
                    "UnitPrice": current_price_decimal,
                    "TotalItemPrice": TotalItemPrice
                })

        # Insert a new order group
        cursor.execute("""
            INSERT INTO OrderGroup (CustomerID, TotalPrice, TransactionDate, Location, OrdersStatus)
            OUTPUT INSERTED.OrderGroupID
            VALUES (?, ?, ?, ?, 'Processing')
        """, (user_id, total_price, datetime.now(), address))
        OrderGroupID = cursor.fetchone()[0]

        # Insert cart items into OrderItem table
        for item in item_details:
            cursor.execute("""
                INSERT INTO OrderItem (OrderGroupID, ProductID, Quantity, TotalItemPrice)
                VALUES (?, ?, ?, ?)
            """, (OrderGroupID, item["ProductID"], item["Quantity"], item["TotalItemPrice"]))

        # Delete the cart items from the ShoppingCart
        cursor.execute("DELETE FROM ShoppingCart WHERE UserID = ?", (user_id,))
        connection.commit()

        return {
            "success": "Checkout completed successfully.",
            "TransactionID": OrderGroupID,
            "item_details": item_details  # Include item details for post-processing
        }

    except pyodbc.Error as e:
        print(f"Database error during checkout: {e}")
        connection.rollback()
        return {"error": "Database error during checkout"}

    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()







# def validate_cart_items(cart_items, user_id, cursor, connection):
#     item_details = []
#     for item in cart_items:
#         ProductID, CartQuantity, UnitPrice = item
#         UnitPrice_decimal = Decimal(UnitPrice)  # Convert string to Decimal for precise calculations

#         # Fetch the current availability and unit price from the product data
#         cursor.execute("""
#             SELECT QuantityAvailable, FORMAT(UnitPrice, 'N2') AS UnitPrice
#             FROM ProductData
#             WHERE ProductID = ?
#         """, (ProductID,))
#         row = cursor.fetchone()
#         if row:
#             available_quantity, current_price = row
#             current_price_decimal = Decimal(current_price)

#             if available_quantity < CartQuantity:
#                 return None, {"error": f"Not enough inventory for ProductID {ProductID}."}

#             if current_price_decimal != UnitPrice_decimal:
#                 cursor.execute("""
#                     UPDATE ShoppingCart
#                     SET UnitPrice = ?
#                     WHERE ProductID = ? AND UserID = ?
#                 """, (current_price, ProductID, user_id))
#                 connection.commit()
#                 return None, {"error": f"Price has changed for ProductID {ProductID}. Updated price in cart from {UnitPrice_decimal} to {current_price_decimal}."}

#             item_details.append({
#                 "ProductID": ProductID,
#                 "Quantity": CartQuantity,
#                 "UnitPrice": current_price_decimal,
#                 "TotalItemPrice": CartQuantity * current_price_decimal  # Calculate total item price here for consistency
#             })

#     return item_details, None


# def checkout_cart(user_id, address):
#     try:
#         connection = connect_db()
#         connection.autocommit = False
#         cursor = connection.cursor()

#         cursor.execute("""
#             SELECT ProductID, CartQuantity, FORMAT(UnitPrice, 'N2') AS UnitPrice
#             FROM ShoppingCart
#             WHERE UserID = ?
#         """, (user_id,))
#         cart_items = cursor.fetchall()

#         if not cart_items:
#             return {"error": "No items in the cart to checkout."}

#         item_details, error = validate_cart_items(cart_items, user_id, cursor, connection)
#         if error:
#             connection.rollback()
#             return error  # Return error if validation failed

#         total_price = sum(item['TotalItemPrice'] for item in item_details)  # Sum total prices of all items

#         cursor.execute("""
#             INSERT INTO OrderGroup (CustomerID, TotalPrice, TransactionDate, Location, OrdersStatus)
#             OUTPUT INSERTED.OrderGroupID
#             VALUES (?, ?, ?, ?, 'Processing')
#         """, (user_id, total_price, datetime.now(), address))
#         OrderGroupID = cursor.fetchone()[0]

#         for item in item_details:
#             cursor.execute("""
#                 INSERT INTO OrderItem (OrderGroupID, ProductID, Quantity, TotalItemPrice)
#                 VALUES (?, ?, ?, ?)
#             """, (OrderGroupID, item["ProductID"], item["Quantity"], item["TotalItemPrice"]))

#         cursor.execute("DELETE FROM ShoppingCart WHERE UserID = ?", (user_id,))
#         connection.commit()

#         return {
#             "success": "Checkout completed successfully.",
#             "TransactionID": OrderGroupID,
#             "item_details": item_details
#         }

#     except pyodbc.Error as e:
#         connection.rollback()
#         return {"error": f"Database error during checkout: {str(e)}"}

#     finally:
#         cursor.close()
#         connection.close()



