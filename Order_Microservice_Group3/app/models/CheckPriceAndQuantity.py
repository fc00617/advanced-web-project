from models.database_connection import connect_db
import pyodbc

from decimal import Decimal



# def check_availability_and_price(product_id, requested_quantity, expected_price):
#     connection = connect_db()
#     try:
#         cursor = connection.cursor()
#         cursor.execute("""
#             SELECT QuantityAvailable, FORMAT(UnitPrice, 'N2') AS UnitPrice
#             FROM ProductData
#             WHERE ProductID = ?
#         """, (product_id,))

#         row = cursor.fetchone()
#         if row:
#             available_quantity, current_price = row
#             current_price_decimal = current_price  
            
#             expected_price_decimal = expected_price

#             response_data = {
#                 "available_quantity": available_quantity,
#                 "current_price": current_price_decimal,
#                 "available": available_quantity >= requested_quantity,
#                 "price_correct": current_price_decimal == expected_price_decimal
#             }

#             messages = []
#             if not response_data["available"]:
#                 messages.append(f"We apologize, but there are only {available_quantity} items available.")

#             if not response_data["price_correct"]:
#                 messages.append(f"The price has changed. The current price is now {current_price_decimal}.")

#             if messages:
#                 response_data["message"] = " ".join(messages)
                
#             return response_data

#         else:
#             return {"available": False, "message": "Product not found."}

#     except pyodbc.Error as e:
#         print(f"Database error in check_availability_and_price: {e}")
#         return {"available": False, "message": f"Database error: {str(e)}"}

#     finally:
#         if cursor:
#             cursor.close()
#         if connection:
#             connection.close()




# def check_availability_and_price(product_id, requested_quantity, expected_price):
#     connection = connect_db()
#     try:
#         cursor = connection.cursor()
#         cursor.execute("""
#             SELECT QuantityAvailable, UnitPrice
#             FROM ProductData
#             WHERE ProductID = ?
#         """, (product_id,))

#         row = cursor.fetchone()
#         if row:
#             available_quantity, current_price = row
#             current_price_float = current_price  # Ensure conversion to float

#             response_data = {
#                 "available_quantity": available_quantity,
#                 "current_price": current_price_float,
#                 "available": available_quantity >= requested_quantity,
#                 "price_correct": current_price_float == expected_price
#             }

#             if not response_data["available"] or not response_data["price_correct"]:
#                 messages = []
#                 if not response_data["available"]:
#                     messages.append(f"We apologize, but there are only {available_quantity} items available.")
#                 if not response_data["price_correct"]:
#                     messages.append(f"The price has changed. The current price is now {current_price_float}.")
#                 response_data["message"] = " ".join(messages)
                
#             return response_data

#         else:
#             return {"available": False, "message": "Product not found."}

#     except pyodbc.Error as e:
#         print(f"Database error in check_availability_and_price: {e}")
#         return {"available": False, "message": f"Database error: {str(e)}"}

#     finally:
#         if cursor:
#             cursor.close()
#         if connection:
#             connection.close()
























def check_availability_and_price(product_id, requested_quantity, expected_price, user_id):
    connection = connect_db()
    try:
        cursor = connection.cursor()
        # Fetch the current available quantity and price
        cursor.execute("""
            SELECT QuantityAvailable, FORMAT(UnitPrice, 'N2') AS UnitPrice
            FROM ProductData
            WHERE ProductID = ?
        """, (product_id,))

        row = cursor.fetchone()
        if row:
            available_quantity, current_price = row
            current_pricefloat = float(current_price)  # Convert formatted price back to float if necessary for comparisons

            response_data = {
                "available_quantity": available_quantity,
                "current_price": current_price,
                "available": available_quantity >= requested_quantity,
                "price_correct": current_pricefloat == expected_price
            }

            messages = []
            if not response_data["available"]:
                messages.append(f"We apologize, but there are only {available_quantity} items available.")

            # Check if the price has changed
            if not response_data["price_correct"]:
                messages.append(f"The price has changed. The current price is now {current_pricefloat}.")
                # Update the ShoppingCart with the new price
                cursor.execute("""
                    UPDATE ShoppingCart
                    SET UnitPrice = ?
                    WHERE ProductID = ? AND UserID = ?
                """, (current_price, product_id, user_id))  
                connection.commit()

            if messages:
                response_data["message"] = " ".join(messages)
                return response_data
            else:
                # All checks passed
                return response_data

        else:
            return {"available": False, "message": "Product not found."}

    except pyodbc.Error as e:
        print(f"Database error in check_availability_and_price: {e}")
        connection.rollback()
        return {"available": False, "message": f"Database error: {str(e)}"}

    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()






# def check_availability_and_price(product_id, requested_quantity, expected_price):
#     connection = connect_db()
#     try:
#         cursor = connection.cursor()
#         cursor.execute("""
#             SELECT QuantityAvailable, FORMAT(UnitPrice, 'N2') AS UnitPrice 
#             FROM ProductData
#             WHERE ProductID = ?
#         """, product_id)

#         row = cursor.fetchone()
#         if row:
#             #The price was stored differently, this may not be an issue with full integration
#             available_quantity, current_price = row
#             current_price = float(current_price)
#             response_data = {
#                 "available_quantity": available_quantity,
#                 "current_price": current_price,
#                 "available": available_quantity >= requested_quantity,
#                 "price_correct": current_price == expected_price
#             }

#             # Construct a message based on availability and price correctness
#             messages = []
#             if not response_data["available"]:
#                 messages.append(f"We apologize, but there are only {available_quantity} items available.")
            
#             if not response_data["price_correct"]:
#                 messages.append(f"The price has changed. The current price is now {current_price}.")

#             if messages:
#                 response_data["message"] = " ".join(messages)
#                 return response_data
#             else:
#                 # All checks passed
#                 return response_data
        
#         else:
#             return {"available": False, "message": "Product not found."}

#     except pyodbc.Error as e:
#         print(f"Database error in check_availability_and_price: {e}")
#         return {"available": False, "message": f"Database error: {str(e)}"}

#     finally:
#         if cursor:
#             cursor.close()
#         if connection:
#             connection.close()