#ReadME
<h1>Folders and Files in the Product Microservice </h1>

 

<h4>1. Controllers/: <h4>

<p>a. productHomeController.py: This page is the landing page of the application. It gets product data from the database by category using a POST method. </p><br>

<p>b. getProductController.py: A user is redirected to this page after they have clicked on a product. It shows information of that product – image, name, price, reviews, ratings. It uses a GET request method to get the product ID. The product ID is sent to the database. </p><br> 

<p>c. addReviewController.py: This section allows a user to add a review to a product. It first of all uses a GET request to get the product ID. It first of all checks if a user's session is active and if they are logged in. If the user is not logged in, it throws an error and asks them to log in.
An HTTP request is also made to the User Microservice to get the username of the user leaving  review. This is because the username is stored in the ReviewsandRatings table to avoid always having to make requests to the User Microservice to retrieve names of users when displaying all the reviews. On the same page, a POST request is also used when a user clicks the button to add the review. After the user has posted the review, the username gotten from the microservice, product ID, review and rating are sent to the database. </p><br>

 

<h4>2. Models/: </h4>

<p>a. productHome.py: This page sends product information by category from the database to "productHomeController.py". </p><br>

<p>b. getProduct.py: This page has a function that collects the product ID from "getProductController.py", it retrieves information about the product using the ID it collected. </p><br> 

<p>c. addRevew.py: This collects review information from "addReviewController.py" and inserts into the ReviewsandRatings table of the product database. </p><br>

<p>d. database_connection.py: This section handles the database connection details for this microservice. </p><br>
 

<h4>3. Config.py:</h4> This file contains the necessary configurations for this microservice - port number, server name. 


<h4>4. Index.py:</h4> This file manages the routing for this microservice. 
