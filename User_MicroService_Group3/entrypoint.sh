#!/bin/bash

# Start SQL Server in the background
/opt/mssql/bin/sqlservr &

# Wait for 5 minutes (300 seconds)
sleep 300

# Log in and restore database
/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P "$SA_PASSWORD" -Q "RESTORE DATABASE User_Management FROM DISK = '/var/opt/mssql/data/User_Management.bak' WITH MOVE 'User_Management.mdf' TO '/var/opt/mssql/data/User_Management.mdf', MOVE 'User_Management_log.ldf' TO '/var/opt/mssql/data/User_Management_log.ldf';"

# Keep the script running indefinitely
wait %1
