#ReadMe

<h2>User Microservice</h2>

<h3>Folders and Files in the User Microservice</h3>

<h5>1. Controllers/:</h5><br>
<p>a. signupController.py: This page collects information from the user when they log sign up. It uses a POST request method. The user's password is encoded using the hashlib and secrets libraries. If no email address is entered, it does not allow sign up. Because email is set to not null in the database.</p><br>
		
<p>b. loginController.py: This page handles user log in and user authentication. It uses a POST request method to collect the user's email address and password. If an email is entered that does not exist, it throws an error to the user. If the email exists in the database, it then checks the entered password. The password is encoded and compared with the encoded password in the database. If it matches, the sign in is successful. A session is then created and is accessible from the server.</p><br>
		
<p>c. logoutController.py: This page handles the logout function and uses a POST request. It first of all checks if the session is active. If it is, it  is deleted from the server.</p><br>
		
<p>d. updateProfileController.py: This section handles changes made to the user's personal information. It uses a POST request method to get the updated information from the user. It does not update the email address because this is constant. It checks if the user's session is active. If it is active, it sends the updated information to the database using the session data. If the session is not active, it throws an error message to the user to log in.</p><br>
		
<p>e. changePasswordController.py: This section handles changing the user's password. It is collected from the user using a POST request. It checks if the user's session is active. If it isn't, it throws an error for the user to log in. If it is, the user's old and new password are collected. The old password is checked against the database. If it matches, the new password is then decoded and updated in the database.</p><br>
		
<p>f. deleteProfileController.py: This section handles deleting a user's profile. It checks if the user's session is active. If it isn't, it throws an error for the user to log in. If the session is active, it calls the database function to delete the user's profile.</p><br>
		
<h5>2. Models/:</h5><br>
<p>a. sigup.py: This collects the user's information from "signupController.py" and checks if the email exists in the database. If the email does not exist, it inserts the user's data in the database.</p><br>
		
<p>b. login.py: This page collects log in information from "loginController.py". It has 2 functions. The first function retrieves the email the user entered if it exists. The second function collects the password and sends it to the "loginController.py" page for verification.</p><br>
		
<p>c. updateProfile.py: This page collects the user's information from "updateProfileController.py" and inserts it into the database.</p><br>
		
<p>d. changePassword.py: This page collects password information from "changePasswordController.py". It has 2 functions. The first function gets the old password, and the second function sets the new password. The second function is called only if the old password matches the password in the database.</p><br>
		
<p>e. deleteProfile.py: This section collects the user's ID from "deleteProfileController.py" and deletes the user's profile if the profile exists.</p><br>
		
<p>f. Database_connection.py: This section handles the database connection details for this microservice.</p><br>
		
<p>All database codes use the try statement for error handling and rolls back changes if not all database operations were successful in a given function.</p><br>
	
<h5>3. Config.py:</h5><p> This file contains the necessary configurations for this microservice - port number, server name</p>
	
<h5>4. Index.py:</h5><p> This file manages the routing for this microservice</p>

